Small console application (.NET, C#) which:
1. Counts number of word in text (base algorithm get https://github.com/allenjp/wordCount)
2. Has 3 options of data input: console, file, SQL database (Firebird Embedded Server 2.5).
Flexible to support additional types of input, (output), processing.
Strategy pattern
bin - https://cloud.mail.ru/public/6a2i/E7HSzzNH6
