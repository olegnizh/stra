/*
 * Created by SharpDevelop.
 * User: alex
 * Date: 14.05.2017
 * Time: 22:56
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;


namespace strategy_app_1
{

	public class WordCountContext
	{
		private IWordCount _IWordCount;
 
        public WordCountContext(IWordCount strategy)
        {
            this._IWordCount = strategy;
        }
 
        public void SetStrategy(IWordCount strategy)
        {
            this._IWordCount = strategy;
        }
 
        public Dictionary<string, int> ExecuteOperation(string s)
        {
        	return this._IWordCount.GetWordCount(s);

        }
	}
}
