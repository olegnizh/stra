/*
 * Created by SharpDevelop.
 * User: alex
 * Date: 14.05.2017
 * Time: 21:51
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;


namespace strategy_app_1
{

	public interface IWordCount
	{
		Dictionary<string, int> GetWordCount(string s);
	}
	
}
