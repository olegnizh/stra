/*
 * Created by SharpDevelop.
 * User: User
 * Date: 15.05.2017
 * Time: 16:02
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


namespace strategy_app_1
{
	/// <summary>
	/// Description of WordCountFromConsole.
	/// </summary>
	/// 
	public class WordCountFromConsole : IWordCount
	{
		public Dictionary<string, int> GetWordCount(string s)
        {               
                    
                    String line = s;

                    // Split string into words
                    String[] words = line.Split(null);

                    // Initialize the key value pair of wordCounts
                    Dictionary<string, int> wordCounts = new Dictionary<string, int>();

                    // Loop through each word in the text
                    for (int i = 0; i < words.Length; i++)
                    {
                        // strip punctuation from the string
                        Regex rgx = new Regex("[^a-zA-Z0-9 -]");
                        words[i] = rgx.Replace(words[i], "");

                        // make case irrelavent
                        words[i] = words[i].ToLower();

                        // if the word already exists in the dictionary,
                        // increase the count by 1
                        if (wordCounts.ContainsKey(words[i]))
                        {
                            wordCounts[words[i]]++;
                        }

                        // if the word does not already exist in the dictionary
                        // add it to the dictionary and set the count to 1                             
                        else
                        {
                            wordCounts.Add(words[i], 1);
                        }
                    }
                    return wordCounts;    
                      
        }
	}

}
