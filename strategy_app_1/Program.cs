/*
 * Created by SharpDevelop.
 * User: alex
 * Date: 14.05.2017
 * Time: 20:39
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


namespace strategy_app_1
{
	class Program
	{
		public static void Main(string[] args)
		{
			if(args.Length != 0)
			{
				string s = "";
				
				WordCountContext context = new WordCountContext(new WordCountFromFile());
				
				if ((args[0] == "--con") && (args.Length > 1))
				{					
					for (int i = 1; i <= args.Length - 1; i++)
						 s = s + " " + args[i];
					//context = new WordCountContext(new WordCountFromConsole());
					context.SetStrategy(new WordCountFromConsole());						
				}
				
				if ((args[0] == "--file") && (args.Length == 2))
				{
					s = args[1];
					//context = new WordCountContext(new WordCountFromFile());
					context.SetStrategy(new WordCountFromFile());				
				}
				
				if ((args[0] == "--db") && (args.Length == 2))
				{					
					s = args[1];
					//context = new WordCountContext(new WordCountFromDb());
					context.SetStrategy(new WordCountFromDb());									
				}
				
				Dictionary<string, int> unsorted = context.ExecuteOperation(s);
				var sortedDict = from entry in unsorted orderby entry.Value ascending select entry;
                foreach (KeyValuePair<string, int> kvp in sortedDict)
                      Console.WriteLine(kvp.Key + " " + kvp.Value);
			
                Console.Write("Press any key to continue . . . ");
		        Console.ReadKey(true);	
				
			}
			else
			{
				Console.WriteLine("Program ver.01 :");
				Console.WriteLine("Usage :");
				Console.WriteLine("arg 1 as --con or --file or --db");
				Console.WriteLine("arg 2 as char or path or connectstring");
				Console.WriteLine("Examples :");
			    Console.WriteLine("strategy_app_1.exe --con ol fkjegh fehbr dx");
			    Console.WriteLine("strategy_app_1.exe --file ..//../text1.txt");
			    Console.WriteLine("strategy_app_1.exe --db User=SYSDBA;Password=Masterkey;Database=C:\\strategy_app_1\\db\\DB1.FDB;DataSource=;ServerType=1");			    
			    Console.Write("Press any key to continue . . . ");
			    Console.ReadKey(true);
			}
			
						
		}
	}
}