/*
 * Created by SharpDevelop.
 * User: User
 * Date: 16.05.2017
 * Time: 8:31
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using FirebirdSql.Data.FirebirdClient;
using System.Data;


namespace strategy_app_1
{
	/// <summary>
	/// Description of WordCountFromDb.
	/// </summary>
	public class WordCountFromDb : IWordCount
	{

		public Dictionary<string, int> GetWordCount(string connectstring)
        {
			String str = "";
			
			// proba connect
			FbConnection con = new FbConnection(connectstring);
			try
            {                
                con.Open();
            }
            catch (FbException err)
            {
               if (con != null)
	    	   {
	    	       if (con.State != ConnectionState.Closed)
	    	           con.Close();
	    	       con.Dispose();
                   con = null;     
	    	   }
               
               Console.WriteLine("Not connect to db: ");
               Console.WriteLine(err.Message);
               Dictionary<string, int> blank = new Dictionary<string, int>();
               return blank;
            }
            
            //get string
            FbCommand cmd = new FbCommand("SELECT COMMENT FROM TABLE1 WHERE ID=@ID", con);
			cmd.Parameters.AddWithValue("@ID", 1);
			FbDataReader rd = cmd.ExecuteReader();			   	       
			if (rd.Read())
                str = rd["COMMENT"].ToString();			   	       
			rd.Close(); 
			cmd.Dispose();
			//con.Close();
			con.Dispose();				                        	                                	

                    // Split the file into words
                    String[] words = str.Split(null);

                    // Initialize the key value pair of wordCounts
                    Dictionary<string, int> wordCounts = new Dictionary<string, int>();

                    // Loop through each word in the text
                    for (int i = 0; i < words.Length; i++)
                    {
                        // strip punctuation from the string
                        Regex rgx = new Regex("[^a-zA-Z0-9 -]");
                        words[i] = rgx.Replace(words[i], "");

                        // make case irrelavent
                        words[i] = words[i].ToLower();

                        // if the word already exists in the dictionary,
                        // increase the count by 1
                        if (wordCounts.ContainsKey(words[i]))
                        {
                            wordCounts[words[i]]++;
                        }

                        // if the word does not already exist in the dictionary
                        // add it to the dictionary and set the count to 1                             
                        else
                        {
                            wordCounts.Add(words[i], 1);
                        }
                    }
                    return wordCounts;
           
                      
        }

	}
}
